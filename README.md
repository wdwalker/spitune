# SPiTune
SPiTune provides you with visual and aural feedback about the tuning of your piano. It's based primarily on modeling the strings with an attempt to combine real-world physics with the practices of aural piano tuners.

## Quick Start
### To run the app
The app runs entirely in the browser, you need to launch a server that offers up the app. Run these commands and then point your browser to [http://localhost:3000/](http://localhost:3000/):

```
npm install
npm start
```


### To generate a model.json file from recordings
The author has made recordings of his Yamaha C3 grand piano, which each key found in a separate file. You can use these files as a means to experiment with SPiTune if you do not have any data to work with.

```
tar xvzf recordings.tar.gz
cd util
tsc --lib es6,dom -t es5 generateModel.ts && node generateModel.js
tsc --lib es6,dom -t es5 currentTuning.ts && node currentTuning.js
```

## Background

* [Equal Temperament](https://en.wikipedia.org/wiki/Equal_temperament) - the most common tuning system used by the western world. Read also about the [Pythagorean comma](https://en.wikipedia.org/wiki/Pythagorean_comma) for background on why equal temperament is used.
* [Inharmonicity](https://en.wikipedia.org/wiki/Inharmonicity) - why real piano strings do not follow the mathematical theory of harmony and why 'stretch tuning' is needed. For our purposes, we call the mathematical series the [harmonic series](https://en.wikipedia.org/wiki/Harmonic_series_(music)) and the inharmonic series the partial series.
* [Railsback Curve](https://en.wikipedia.org/wiki/Piano_acoustics#The_Railsback_curve) - from 1937, an early look at the difference between normal piano tuning and an equal-tempered scale. O.L. Railsback noticed the 'stretch tuning' of piano tuners and created his famous smooth "Railsback Curve" to map the trend. Unforunately, no single curve maps to all pianos.
* [The Inharmonicity of Musical String Instruments (1543-1993)](http://www.patriziobarbieri.it/pdf/inharmonicity.pdf) - a good survey of inharmonicity investigations done over several centuries.
* [The Inharmonicity of Piano Strings](http://www.simonhendry.co.uk/wp/wp-content/uploads/2012/08/inharmonicity.pdf) - Simon Hendry's 2008 Masters Thesis which provides a somewhat practical approach.
* [Sensations of Tone](https://en.wikipedia.org/wiki/Sensations_of_Tone) - seminal work from 1863 by Hermann von Helmholtz.
* [Tonal Consonance and Critical Bandwidth](http://www.mpi.nl/world/materials/publications/levelt/Plomp_Levelt_Tonal_1965.pdf) - Plomp and Levelt's research from 1965 that digs into consonant and dissonant intervals.
* [Local Consonance and the Relationship Between Timbre and Scale](http://ib.adnxs.com/seg?add=1&redir=https%3A%2F%2Fwww.researchgate.net%2Fprofile%2FWilliam_Sethares%2Fpublication%2F28621436_Local_consonance_and_the_relationship_between_timbre_and_scale%2Flinks%2F588cc04045851567c93e18e4%2FLocal-consonance-and-the-relationship-between-timbre-and-scale.pdf%3Forigin%3Dpublication_detail) - following Plomp and Levelt, thoughts from Sethares in 1993 on consonance vs. dissonance.
* [Explaining the Railsback Stretch in Terms of the Inharmonicity of Piano Tones and Sensory Sissonance](http://asa.scitation.org/doi/pdf/10.1121/1.4931439) - N. Giordano's 2015 paper that combines the perceptual work from a number of the above (e.g., Plomp and Levelt, Sethares). It demonstrates how mathematically derived dissonance/consonance from an aurally tuned piano 'explains' the Railsback curve.
