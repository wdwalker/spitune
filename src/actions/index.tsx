import { KeyName } from '../util/Piano';
import Tuning from '../util/Tuning';
import InharmonicityModel from '../util/InharmonicityModel';

export enum ActionKeys {
  NEW_SAMPLES = 'NEW_SAMPLES',
  SET_SNIPPET = 'SET_SNIPPET',
  SET_LISTENING = 'SET_LISTENING',
  SET_PLAYING = 'SET_PLAYING',
  SET_ACTIVE = 'SET_ACTIVE',
  SET_PROCESSING = 'SET_PROCESSING',
  SET_SELECTED_KEY = 'SET_SELECTED_KEY',
  SET_MODEL = 'SET_MODEL',
  SET_TUNING = 'SET_TUNING',
  TUNE_KEY = 'TUNE_KEY',
  OTHER = '__any_other_action_type__'
}

// We have new samples from our recording
export interface NewSamples {
  type: ActionKeys.NEW_SAMPLES;
  samples: Float32Array;
}

export const newSamples = (samples: Float32Array) => ({
  samples,
  type: ActionKeys.NEW_SAMPLES
});

// Our endpointed audio snippet
export interface SetSnippet {
  type: ActionKeys.SET_SNIPPET;
  snippet: Float32Array;
}

export const setSnippet = (snippet: Float32Array) => ({
  snippet,
  type: ActionKeys.SET_SNIPPET
});

// We want to enable or disable the microphone
export interface SetListening {
  type: ActionKeys.SET_LISTENING;
  value: boolean;
}

export const setListening = (value: boolean) => ({
  value,
  type: ActionKeys.SET_LISTENING
});

// We want to enable or disable the bandpass playback
export interface SetPlaying {
  type: ActionKeys.SET_PLAYING;
  value: boolean;
}

export const setPlaying = (value: boolean) => ({
  value,
  type: ActionKeys.SET_PLAYING
});

// Do we care about the audio?
export interface SetActive {
  type: ActionKeys.SET_ACTIVE;
  value: boolean;
}

export const setActive = (value: boolean) => ({
  value,
  type: ActionKeys.SET_ACTIVE
});

// We are busy doing the preciseFFT analysis.
export interface SetProcessing {
  type: ActionKeys.SET_PROCESSING;
  value: boolean;
}

export const setProcessing = (value: boolean) => ({
  value,
  type: ActionKeys.SET_PROCESSING
});

// The user selected a key on the keyboard.
export interface SetSelectedKey {
  type: ActionKeys.SET_SELECTED_KEY;
  keyName: KeyName;
}

export const setSelectedKey = (keyName: KeyName) => ({
  keyName,
  type: ActionKeys.SET_SELECTED_KEY
});

// We want to set the model the whole piano.
export interface SetModel {
  type: ActionKeys.SET_MODEL;
  model: InharmonicityModel;
}

export const setModel = (model: InharmonicityModel) => ({
  model,
  type: ActionKeys.SET_MODEL
});

// We want to set the tuning for the whole piano.
export interface SetTuning {
  type: ActionKeys.SET_TUNING;
  tuning: Tuning;
}

export const setTuning = (tuning: Tuning) => ({
  tuning,
  type: ActionKeys.SET_TUNING
});

// We want to set the tuning for a given key.
export interface TuneKey {
  type: ActionKeys.TUNE_KEY;
  keyName: KeyName;
  cents: number;
}

export const tuneKey = (keyName: KeyName, cents: number) => ({
  keyName,
  cents,
  type: ActionKeys.TUNE_KEY
});

export type ActionTypes =
  | NewSamples
  | SetSnippet
  | SetListening
  | SetPlaying
  | SetActive
  | SetProcessing
  | SetSelectedKey
  | SetModel
  | SetTuning
  | TuneKey;
