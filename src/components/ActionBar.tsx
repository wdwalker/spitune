import * as React from 'react';

import UploadModel from '../containers/UploadModel';
import SaveModel from '../containers/SaveModel';
import UploadTuning from '../containers/UploadTuning';
import SaveTuning from '../containers/SaveTuning';
import SaveAudio from '../containers/SaveAudio';
import UploadAudio from '../containers/UploadAudio';
import PlayAudio from '../containers/PlayAudio';
import AudioController from '../containers/AudioController';

import './ActionBar.css';

/**
 * The navigation bar across the top of the display.
 */
const NavBar: React.SFC = () => {
  const route = location ? location.pathname.replace(/^.*(\\|\/|:)/, '') : '';

  return (
    <div id="action-bar">
      <AudioController />
      {(route === 'frequency' || route === 'model') && <PlayAudio />}
      {(route === 'frequency' || route === 'model') && <UploadAudio />}
      {(route === 'frequency' || route === 'model') && <SaveAudio />}
      {route === 'model' && <UploadModel />}
      {route === 'model' && <SaveModel />}
      {route === 'tune' && <UploadTuning />}
      {route === 'tune' && <SaveTuning />}
    </div>
  );
};

export default NavBar;
