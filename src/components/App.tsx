import * as React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

import NavBar from './NavBar';
import ActionBar from './ActionBar';
import Home from './Home';
import BeatFilter from '../containers/BeatFilter';
import FrequencyAnalyzer from '../containers/FrequencyAnalyzer';
import PianoModeler from '../containers/PianoModeler';
import PianoTuner from '../containers/PianoTuner';

import './App.css';

interface Props {
  processing: boolean;
}

/**
 * Our top level container.
 */
const App: React.SFC<Props> = (props) => {
  return (
    <div className="app">
      {props.processing && <div className="busy" />}
      <NavBar />
      <ActionBar />
      <Switch>
        <Route exact={true} path="/" component={Home} />
        <Route path="/beats" component={BeatFilter} />
        <Route path="/frequency" component={FrequencyAnalyzer} />
        <Route path="/model" component={PianoModeler} />
        <Route path="/tune" component={PianoTuner} />
        <Redirect to="/" />
      </Switch>
    </div>
  );
};

export default App;
