import * as React from 'react';

import AudioHelper, { Callback } from '../util/AudioHelper';

import './AudioController.css';

interface Props {
  audioHelper: AudioHelper;
  listening: boolean;
  setListening: (value: boolean) => {};
  newSamples: (samples: Float32Array) => {};
  playing: boolean;
  setPlaying: (value: boolean) => {};
  active: boolean;
  setActive: (value: boolean) => {};
}

/**
 * Displays the buttons to enable/disable the microphone and
 * playback. Also handles audio coming from the microphone.
 */
class AudioController extends React.Component<Props> {
  constructor(props: Props) {
    super(props);
    this.onAudio = this.onAudio.bind(this);
  }

  /**
   * We have new audio from the microphone. During the quiet moments, we keep
   * a 2sec moving average of the RMS. We then use this as a threshold for
   * when we start and stop caring about the audio coming in on the mic.
   */
  onAudio: Callback = (samples: Float32Array) => {
    this.props.newSamples(samples);
    this.props.setActive(this.props.audioHelper.active);
  };

  /**
   * Downloads the current audio samples as [keyname].json.
   * @param keyName the keyname
   * @param samples the samples
   */
  download(keyName: string, samples: Float32Array) {
    const wav = this.props.audioHelper.toWav(samples);
    const blob = new window.Blob([new DataView(wav)], {
      type: 'audio/wav'
    });
    const anchor = document.createElement('a');
    const url = window.URL.createObjectURL(blob);
    anchor.href = url;
    anchor.download = `${keyName}.wav`;
    anchor.click();
    window.URL.revokeObjectURL(url);
  }

  render() {
    const {
      audioHelper,
      listening,
      setListening,
      playing,
      setPlaying,
      active,
      setActive
    } = this.props;
    const playTitle = playing ? 'Turn off playback' : 'Turn on playback';
    const listenTitle = listening ? 'Turn off listening' : 'Turn on listening';
    return (
      <div id="audio-controller">
        <button
          title={listenTitle}
          id="listen-button"
          type="button"
          className={`btn ${listening ? 'active' : ''} ${
            active ? 'threshold' : ''
          }`}
          data-toggle="button"
          aria-pressed={listening}
          onClick={(e) => {
            if (!listening) {
              audioHelper.startRecording(this.onAudio);
            } else {
              audioHelper.stopRecording();
            }
            if (active) {
              setActive(!active);
            }
            setListening(!listening);
          }}
        >
          <span className="fa fa-microphone audio-button" />
        </button>
        <button
          title={playTitle}
          id="play-button"
          type="button"
          className={`btn ${playing ? 'active' : ''}`}
          data-toggle="button"
          aria-pressed={playing}
          onClick={(e) => {
            setPlaying(!playing);
          }}
        >
          <span className="fa fa-volume-up audio-button" />
        </button>
      </div>
    );
  }
}

export default AudioController;
