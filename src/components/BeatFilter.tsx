import * as React from 'react';
import DSP from 'digitalsignals';

import EqualTemperament from '../util/EqualTemperament';
import { KeyName } from '../util/Piano';
import AudioHelper from '../util/AudioHelper';
import * as stats from '../util/stats';
import * as draw from '../util/draw';

import Keyboard from '../containers/Keyboard';

import './BeatFilter.css';

interface Props {
  audioHelper: AudioHelper;
  samples: Float32Array;
  playing: boolean;
  active: boolean;
  selectedKey: KeyName;
  setSelectedKey: (keyName: KeyName) => {};
}

/**
 * Our "Beat Filter" page that displays and plays the bandpass analysis.
 */
class BeatFilter extends React.Component<Props> {
  /**
   * Our bandpass filter info
   */
  private bandpassFilter: any; // tslint:disable-line

  /**
   * Our svg drawing area for D3
   */
  private svgElement: SVGSVGElement;
  private readonly DOMAIN_MAX: number = 0.2; // vertical range

  /**
   * How many seconds of filtered data to draw
   */
  private readonly SECONDS: number = 2;
  private filtered: number[]; // SECONDS of accumulated filtered signal

  /**
   * For our RMS calculations
   */
  private readonly RMS_WINDOW_SIZE = 1024; // # of samples
  private readonly ROLLING_AVG_WINDOW_SIZE = 1024; // # of samples
  private f2: number[]; // The squares of samples in filtered
  private f2sum: number; // a running sum of the last RMS_WINDOW_SIZE f2 values
  private rms: number[]; // The rms of the last RMS_WINDOW_SIZE values
  private confidentBPS: number;

  constructor(props: Props) {
    super(props);
    this.bandpassFilter = new DSP.Biquad(
      DSP.BPF_CONSTANT_PEAK,
      this.props.audioHelper.sampleRate
    );
    this.bandpassFilter.setQ(90);
    this.bandpassFilter.setF0(EqualTemperament.hz(this.props.selectedKey));
    this.filtered = [];
    this.f2 = [];
    this.f2sum = 0;
    this.rms = [];
    this.confidentBPS = 0;
  }

  /**
   * Draws the filtered signal
   * @param signal the filtered samples
   * @param color  what color to use
   * @param offset index we should use as the beginning of the signal
   */
  drawSignal(signal: number[], color: string = '#00ffff', offset: number = 0) {
    draw.drawSignal(
      this.svgElement,
      signal,
      [0, this.SECONDS * this.props.audioHelper.sampleRate],
      [-this.DOMAIN_MAX, this.DOMAIN_MAX],
      color,
      offset
    );
  }

  /**
   * Draws the maxima of the filtered signal
   * @param signal the filtered samples
   * @param maxima array of indexes into signal
   * @param color  what color to use
   * @param offset index we should use as the beginning of the signal
   */
  drawSignalMaxima(
    signal: number[],
    maxima: number[],
    color: string = 'blue',
    offset: number = 0
  ) {
    draw.drawSignalMaxima(
      this.svgElement,
      signal,
      maxima,
      [0, this.SECONDS * this.props.audioHelper.sampleRate],
      [-this.DOMAIN_MAX, this.DOMAIN_MAX],
      color,
      offset
    );
  }

  /**
   * Append the filtered data to our accumulated arrays, calculating
   * the rolling RMS as soon as we have enough samples
   * @param filtered
   */
  accumulateAndTrim(filtered: Float32Array) {
    for (let i = 0; i < filtered.length; i += 1) {
      this.filtered[this.filtered.length] = filtered[i];
      this.f2[this.f2.length] = filtered[i] * filtered[i];
      this.f2sum += this.f2[this.f2.length - 1];
      if (this.f2.length >= this.RMS_WINDOW_SIZE) {
        this.rms[this.rms.length] = Math.sqrt(
          this.f2sum / this.RMS_WINDOW_SIZE
        );
        this.f2sum -= this.f2[this.f2.length - this.RMS_WINDOW_SIZE];
      }
    }

    // Trim our accumulated arrays down to SECONDS worth of samples
    if (
      this.filtered.length >=
      this.SECONDS * this.props.audioHelper.sampleRate
    ) {
      this.filtered = this.filtered.slice(
        -(this.SECONDS * this.props.audioHelper.sampleRate)
      );
      this.f2 = this.f2.slice(
        -(this.SECONDS * this.props.audioHelper.sampleRate)
      );
    }
    if (this.rms.length >= this.SECONDS * this.props.audioHelper.sampleRate) {
      this.rms = this.rms.slice(
        -(this.SECONDS * this.props.audioHelper.sampleRate)
      );
    }
  }

  /**
   * Compute the BPS, updating this.confidentBPS if we have enough
   * good data.
   */
  computeBPS() {
    // Smooth out the rms with a rolling average
    const rolling = stats.rollingAverage(
      this.rms,
      this.rms[0],
      this.ROLLING_AVG_WINDOW_SIZE
    );

    // Find some peaks and cleanse them - there should never be a
    // maxima that's less than both the one before and the one
    // after it, and we discard maxima that are too close to
    // the one after it (2201 ~= 20bps)
    let maxima = stats.criticalPoints(rolling).maxima;
    maxima = maxima.filter((x, i) => {
      if (i > 0 && i < maxima.length - 1) {
        return (
          rolling[maxima[i]] > 0.01 &&
          !(
            rolling[maxima[i - 1]] >= rolling[maxima[i]] &&
            rolling[maxima[i + 1]] >= rolling[maxima[i]]
          ) &&
          maxima[i + 1] - maxima[i] > 2201
        );
      }
      return rolling[maxima[i]] > 0.01;
    });

    // Draw the points for the peaks on top of the rolling RMS
    this.drawSignal(this.rms, 'red', this.RMS_WINDOW_SIZE / 2);
    this.drawSignalMaxima(
      this.rms,
      maxima,
      'red',
      (this.ROLLING_AVG_WINDOW_SIZE + this.RMS_WINDOW_SIZE) / 2
    );

    // Let's compute some BPS based upon the filtered maxima.
    // If we get more than 5 bps values and our interquartile
    // range is less than 0.5bps, we'll say we have a pretty
    // good guess.
    const bps: number[] = [];
    if (maxima.length > 5) {
      for (let i = 1; i < maxima.length; i += 1) {
        bps[bps.length] =
          this.props.audioHelper.sampleRate / (maxima[i] - maxima[i - 1]);
      }
      const quartiles = stats.quartiles(
        bps.sort((a: number, b: number) => a - b)
      );
      if (quartiles.q3 - quartiles.q1 < 0.5) {
        this.confidentBPS = quartiles.q2;
      }
    }
  }

  componentDidUpdate(prevProps: Props) {
    // If the user selected a new key, update the F0 of our bandpass filter
    if (this.props.selectedKey !== prevProps.selectedKey) {
      this.bandpassFilter.setF0(EqualTemperament.hz(this.props.selectedKey));
    }

    // When we start listening, clear our confidentBPS result
    if (this.props.active && !prevProps.active) {
      this.confidentBPS = 0;
    }

    /**
     * Play and plot the bandpass-filtered waveform.
     */
    if (this.props.samples.length && this.props.samples !== prevProps.samples) {
      // Apply the bandpass filter to the raw samples
      const filtered: Float32Array = this.bandpassFilter.process(
        this.props.samples
      );

      // Play it.
      if (this.props.playing) {
        this.props.audioHelper.play(filtered);
      }

      // Draw it and do some BPS analysis
      this.accumulateAndTrim(filtered);
      if (this.props.active) {
        // Clear out whatever is there now and draw the filtered signal
        const svgElement = document.getElementById('filtered-signal');
        while (svgElement && svgElement.firstChild) {
          svgElement.removeChild(svgElement.firstChild);
        }
        this.drawSignal(this.filtered);
        this.computeBPS();
      }
    }
  }

  render() {
    return (
      <main id="beat-filter-page">
        <div className="container">
          <div id="beat-filter-bps">
            {this.confidentBPS.toFixed(2)}bps @ {this.props.selectedKey}
          </div>
          <div id="signal-wrapper">
            <svg
              id="filtered-signal"
              ref={(ref: SVGSVGElement) => (this.svgElement = ref)}
            />
          </div>
          <Keyboard />
        </div>
      </main>
    );
  }
}

export default BeatFilter;
