import * as React from 'react';

import FFT from '../util/FFT';
import { KeyName } from '../util/Piano';
import InharmonicityModel from '../util/InharmonicityModel';
import EqualTemperament from '../util/EqualTemperament';

import AudioHelper from '../util/AudioHelper';

import Keyboard from '../containers/Keyboard';

import './FrequencyAnalyzer.css';

export interface Props {
  audioHelper: AudioHelper;
  samples: Float32Array;
  snippet: Float32Array;
  model: InharmonicityModel;
  coarseFFT: FFT;
  preciseFFT: FFT;
  playing: boolean;
  active: boolean;
  selectedKey: KeyName;
  setSnippet: (snippet: Float32Array) => {};
  setProcessing: (value: boolean) => {};
  setSelectedKey: (keyName: string) => {};
}

const float32Concat = (first: Float32Array, second: Float32Array) => {
  const firstLength = first.length;
  const result = new Float32Array(firstLength + second.length);
  result.set(first);
  result.set(second, firstLength);
  return result;
};

/**
 * Our "Frequency Analyzer" page that shows the spectrum across the whole
 * keyboard and also the zoomed spectrum for an individual key. Will also
 * compute the BPS between the two top peaks in the zoomed area.
 */
class FrequencyAnalyzer extends React.Component<Props> {
  /**
   * Our peak Hz and BPS
   */
  protected peakHz: number;
  protected keyHz: number;
  protected bps: number;

  /**
   * Our svg drawing area for D3
   */
  protected fullSpectrumSVG: SVGSVGElement;
  protected keySpectrumSVG: SVGSVGElement;

  protected accumulatedSamples: Float32Array;

  constructor(props: Props) {
    super(props);
    this.doPrecise = this.doPrecise.bind(this);

    this.accumulatedSamples = new Float32Array(0);
    this.peakHz = 0;
    this.keyHz = 0;
    this.bps = 0;
  }

  /**
   * Draws the full spectrum and the zoomed spectrum.
   * @param fft
   */
  drawSpectra(fft: FFT) {
    [this.peakHz] = this.fullSpectrumSVG ? fft.draw(this.fullSpectrumSVG) : [0];
    const peakHz = this.keySpectrumSVG
      ? fft.draw(this.keySpectrumSVG, this.props.selectedKey)
      : [0];
    this.bps = peakHz.length > 1 ? peakHz[0] - peakHz[1] : 0;
    this.keyHz = peakHz.length ? peakHz[0] : 0;
    this.props.setProcessing(false);
  }

  /**
   * Our coarse FFT computation that is done for each chunk of audio we get.
   */
  doCoarse() {
    this.props.coarseFFT.process(this.props.samples);
    this.drawSpectra(this.props.coarseFFT);
  }

  /**
   * To be used by subclasses; called after the FFT calculation has been done,
   * but before the spectra are drawn.
   */
  preciseHook() {
    return;
  }

  /**
   * Do our precise FFT and plot it.
   */
  doPrecise() {
    // Do the FFT
    this.props.preciseFFT.reset();
    this.props.preciseFFT.process(this.props.snippet);
    // const fftInput = this.props.preciseFFT.process(this.props.snippet);
    this.preciseHook();
    this.drawSpectra(this.props.preciseFFT);

    if (this.props.playing) {
      this.props.audioHelper.play(this.props.snippet);
      // this.props.audioHelper.play(fftInput);
    }
  }

  componentDidUpdate(prevProps: Props) {
    // On changes to active (meaning we've gone inside or outside an audio
    // endpoint), we will either start accumulating samples or process what
    // we've accumulated.
    if (this.props.active !== prevProps.active) {
      if (this.props.active) {
        this.accumulatedSamples = this.props.audioHelper.trim(
          this.props.samples
        );
      } else {
        this.props.setSnippet(this.accumulatedSamples);
      }
    }

    // Do a precise FFT analysis on the snippet we finished above.
    if (this.props.snippet && this.props.snippet !== prevProps.snippet) {
      this.props.setProcessing(true);
      setTimeout(this.doPrecise, 20);
    }

    // Each time we get something while inside an endpoint, we plot a coarse
    // FFT and also keep accumulating samples for our snippet to process with
    // our precise FFT.
    if (
      this.props.active &&
      this.props.samples.length &&
      this.props.samples !== prevProps.samples
    ) {
      this.accumulatedSamples = float32Concat(
        this.accumulatedSamples,
        this.props.samples
      );
      this.doCoarse();
    }

    // We allow the user to hit different keys to see a zoomed up spectrum
    // of the snippet we processed with the precise FFT.
    if (this.props.selectedKey !== prevProps.selectedKey) {
      this.props.setProcessing(true);
      setTimeout(() => this.drawSpectra(this.props.preciseFFT), 20);
    }
  }

  render() {
    let fullSpectrumInfo = 'Record a note to see the spectrum';
    if (this.peakHz > 0) {
      const kac = EqualTemperament.keyAndCents(this.peakHz);
      fullSpectrumInfo = `Peak: ${this.peakHz.toFixed(2)}Hz (${kac.keyName} ${
        kac.cents > 0 ? '+' : ''
      }${kac.cents.toFixed(1)}c)`;
    }
    let keySpectrumInfo = 'Record a note to see the spectrum';
    if (this.keyHz > 0) {
      const kac = EqualTemperament.keyAndCents(this.keyHz);
      keySpectrumInfo = `Peak: ${this.keyHz.toFixed(2)}Hz (${kac.keyName} ${
        kac.cents > 0 ? '+' : ''
      }${kac.cents.toFixed(1)}c) - ${this.bps.toFixed(2)}bps`;
    }
    return (
      <main id="frequency-analyzer-page">
        <div className="container">
          <div id="peak-hz">{fullSpectrumInfo}</div>
          <div id="spectrum-wrapper">
            <svg
              id="full-spectrum"
              ref={(ref: SVGSVGElement) => {
                this.fullSpectrumSVG = ref;
              }}
            />
          </div>
          <Keyboard />
          <div id="key-spectrum-info">{keySpectrumInfo}</div>
          <div id="key-spectrum-wrapper">
            <svg
              id="key-spectrum"
              ref={(ref: SVGSVGElement) => (this.keySpectrumSVG = ref)}
            />
          </div>
        </div>
      </main>
    );
  }
}

export default FrequencyAnalyzer;
