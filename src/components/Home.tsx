import * as React from 'react';
import { Link } from 'react-router-dom';

import './Home.css';

/**
 * Our "SPiTune" landing page.
 */
const Home: React.SFC = () => {
  return (
    <main className="home-page">
      <div className="jumbotron">
        <div className="container">
          <h1 className="title">Welcome!</h1>
          <p className="lead">
            SPiTune provides you with visual and aural feedback about the tuning
            of your piano. On the upper right, you will find a microphone
            control to tell SPiTune to listen to your piano and a speaker
            control to tell SPiTune to provide you with audible feedback as it
            analyzes what it hears. It's best to use headphones with SPiTune to
            avoid having the microphone pick up what SPiTune plays. On the upper
            left, you will find quick navigation links to the items described
            below.
          </p>
        </div>
        <div className="container summary">
          <div className="row">
            <div className="col-md-3">
              <h2>Filter</h2>
              <p>
                The <Link to="/beats">Beat Filter</Link> lets you hear the beats
                produced by an interval. Select both the microphone and speaker
                icons are selected in the upper right and select the key where
                the beating will occur. Then, play an interval. SPiTune will
                filter the audio so you can hear the beats at the key you
                selected. For example, select A5 and then play F3:A3 on your
                piano. You should both see and hear about 7 beats per second.
              </p>
            </div>
            <div className="col-md-3">
              <h2>Tune</h2>
              <p>
                The <Link to="/tune">Piano Tuner</Link> lets you virtually twist
                the pins on your piano and both see and hear the results. If you
                select the speaker icon and press one or more keys in a row, you
                can hear your tuning. The Piano Tuner provides a strobe tuner at
                the top of the display that listens at the exact tuning you have
                specified for your selected key. Note that this requires a model
                to be loaded into SPiTune (see{' '}
                <Link to="/model">Piano Modeler</Link>).
              </p>
            </div>
            <div className="col-md-3">
              <h2>Analyze</h2>
              <p>
                The <Link to="/tune">Frequency Analyzer</Link> lets you see the
                frequency spectrum of what SPiTune hears. Select the microphone
                icon on the upper right. Then, play and hold a note. SPiTune
                shows a coarse analysis as it listens and then presents a
                precise analysis once the sound of your piano has softened. The
                Frequency Analyzer also performs beat analysis, but does so
                visually in a spectrum zoomed in on the key you have selected.
              </p>
            </div>
            <div className="col-md-3">
              <h2>Model</h2>
              <p>
                All pianos have{' '}
                <a href="https://en.wikipedia.org/wiki/Inharmonicity">
                  inharmonicity
                </a>{' '}
                that makes them deviate from mathematical theory. With the{' '}
                <Link to="/model">Piano Modeler</Link>, you record each string
                of your piano. The Piano Modeler calculates the inharmonicity
                and the relative spectral power for each partial of each string
                you record. The <Link to="/tune">Piano Tuner</Link> uses this
                model both to predict beats as you tune the piano and to
                synthesize the audio it plays when you press a key.
              </p>
            </div>
          </div>
        </div>
        <hr />
        <div className="container summary">
          <p className="lead">
            SPiTune is open software created by Willie Walker. You can find the
            code on <a href="https://github.com/wdwalker/spitune">github</a>. If
            you are a developer, please contribute. The code has also been
            architected to enable the development of software tuning algorithms
            that use the model; perhaps you have an idea here.
          </p>
        </div>
      </div>
    </main>
  );
};

export default Home;
