import * as React from 'react';

import { KeyName, KEY_NAMES } from '../util/Piano';

import './Keyboard.css';

const classNames = {
  C: 'c-key white-key',
  'C#': 'cs-key black-key',
  D: 'd-key white-key',
  'D#': 'ds-key black-key',
  E: 'e-key white-key',
  F: 'f-key white-key',
  'F#': 'fs-key black-key',
  G: 'g-key white-key',
  'G#': 'gs-key black-key',
  A: 'a-key white-key',
  'A#': 'as-key black-key',
  B: 'b-key white-key'
};

interface Props {
  showInterval: boolean;
  selectedKey: KeyName;
  interval: number;
  setSelectedKey: (keyName: KeyName) => {};
  keyClicked: (keyName: KeyName) => void;
}

/**
 * Draws the piano keyboard on the display.
 * NOTE: this is fixed in size and doesn't scale.
 */
class Keyboard extends React.Component<Props> {
  constructor(props: Props) {
    super(props);
    this.createKey = this.createKey.bind(this);
  }
  createKey(keyName: KeyName) {
    const {
      showInterval,
      interval,
      selectedKey,
      setSelectedKey,
      keyClicked
    } = this.props;
    const octave = keyName.slice(-1);
    const name = keyName.slice(0, keyName.length - 1);
    const keyIndex = KEY_NAMES.indexOf(selectedKey);
    const intervalKeyName = KEY_NAMES[keyIndex + interval];
    const className =
      selectedKey === keyName
        ? `${classNames[name]} checked`
        : showInterval && intervalKeyName === keyName
          ? `${classNames[name]} interval`
          : classNames[name];
    return (
      <li key={keyName}>
        <button
          id={keyName}
          className={className}
          onClick={() => {
            setSelectedKey(keyName);
            if (keyClicked) {
              keyClicked(keyName);
            }
          }}
        >
          <div className="keyname">
            {name}
            <sub>{octave}</sub>
          </div>
        </button>
      </li>
    );
  }

  render() {
    return (
      <div id="keyboard-wrapper">
        <ul id="keyboard">{KEY_NAMES.map(this.createKey)}</ul>
      </div>
    );
  }
}

export default Keyboard;
