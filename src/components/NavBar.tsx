import * as React from 'react';
import { Link } from 'react-router-dom';

import './NavBar.css';

/**
 * The navigation bar across the top of the display.
 */
const NavBar: React.SFC = () => {
  const route = location ? location.pathname.replace(/^.*(\\|\/|:)/, '') : '';
  const beatsClass =
    route === 'beats' ? 'nav-item nav-link active' : 'nav-item nav-link';
  const frequencyClass =
    route === 'frequency' ? 'nav-item nav-link active' : 'nav-item nav-link';
  const modelClass =
    route === 'model' ? 'nav-item nav-link active' : 'nav-item nav-link';
  const tuneClass =
    route === 'tune' ? 'nav-item nav-link active' : 'nav-item nav-link';

  return (
    <nav className="navbar fixed-top navbar-expand-sm navbar-dark bg-dark">
      <Link className="navbar-brand" to="/">
        SPiTune
      </Link>
      <button
        className="navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#navbarNavAltMarkup"
        aria-controls="navbarNavAltMarkup"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <span className="navbar-toggler-icon" />
      </button>
      <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div className="navbar-nav">
          <Link className={beatsClass} to="/beats">
            Filter
          </Link>
          <Link className={tuneClass} to="/tune">
            Tuner
          </Link>
          <Link className={frequencyClass} to="/frequency">
            Analyzer
          </Link>
          <Link className={modelClass} to="/model">
            Modeler
          </Link>
        </div>
      </div>
    </nav>
  );
};

export default NavBar;
