import * as React from 'react';

import EqualTemperament from '../util/EqualTemperament';
import Keyboard from '../containers/Keyboard';
import FrequencyAnalyzer, { Props } from './FrequencyAnalyzer';

import './PianoModeler.css';

/**
 * Our "Piano Modeler" page that will gather partial frequencies for a note
 * and save them in the piano model.
 */
class PianoModeler extends FrequencyAnalyzer {
  private partials: number[];

  constructor(props: Props) {
    super(props);
    this.partials = [];
  }

  /**
   * Computes the partials and saves them in our model.
   */
  preciseHook() {
    const keyName = this.props.selectedKey;
    this.props.model.set(
      keyName,
      this.props.model.generate(keyName, this.props.preciseFFT)
    );
    this.partials = this.props.model.get(keyName).fin;
    console.log(JSON.stringify(this.props.model.get(keyName), null, '  '));
    this.props.setProcessing(false);
  }

  /**
   * Returns a human-consumable string that represents the key and
   * cents offset from equal temperament tuning for the given Hz.
   * @param hZ the frequency
   */
  kac(hZ: number) {
    const keyAndCents = EqualTemperament.keyAndCents(Math.abs(hZ));
    return `(${keyAndCents.keyName} ${
      keyAndCents.cents > 0 ? '+' : ''
    }${keyAndCents.cents.toFixed(1)}c)`;
  }

  /**
   * Returns the name of the key associated with the given frequency.
   * @param hZ the frequency
   */
  keyName(hZ: number) {
    const keyAndCents = EqualTemperament.keyAndCents(Math.abs(hZ));
    return keyAndCents.keyName;
  }

  render() {
    let fullSpectrumInfo = 'Record a note to see the spectrum';
    if (this.peakHz > 0) {
      fullSpectrumInfo = `Peak: ${this.peakHz.toFixed(2)}Hz ${this.kac(
        this.peakHz
      )}`;
    }
    let keySpectrumInfo = 'Record a note to see the spectrum';
    if (this.keyHz > 0) {
      keySpectrumInfo = `Peak: ${this.keyHz.toFixed(2)}Hz ${this.kac(
        this.keyHz
      )}`;
    }
    return (
      <main id="piano-modeler-page">
        <div className="container">
          <div id="peak-hz">{fullSpectrumInfo}</div>
          <div id="spectrum-wrapper">
            <svg
              id="full-spectrum"
              ref={(ref: SVGSVGElement) => {
                this.fullSpectrumSVG = ref;
                // this.drawSpectra(this.props.preciseFFT);
              }}
            />
          </div>
          <Keyboard />
          <div id="model-partials-container">
            <div id="model-info-container">
              <div id="model-spectrum-info">{keySpectrumInfo}</div>
              <div id="model-spectrum-wrapper">
                <svg
                  id="model-spectrum"
                  ref={(ref: SVGSVGElement) => (this.keySpectrumSVG = ref)}
                />
              </div>
            </div>
            <div id="partials">
              <ul id="partials-list">
                {this.partials.map((val, i) => (
                  <li
                    onClick={() => this.props.setSelectedKey(this.keyName(val))}
                    className={val < 0 ? 'unconfident' : ''}
                  >
                    P{i + 1}: {Math.abs(val).toFixed(2)}Hz {this.kac(val)}
                  </li>
                ))}
              </ul>
            </div>
          </div>
        </div>
      </main>
    );
  }
}

export default PianoModeler;
