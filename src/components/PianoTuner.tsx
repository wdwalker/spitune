import * as React from 'react';
import DSP from 'digitalsignals';

import * as draw from '../util/draw';

import {
  KeyName,
  KEY_NAMES,
  PARTIAL_STEPS,
  COMMON_PARTIALS,
  INTERVAL_NAMES
} from '../util/Piano';
import InharmonicityModel from '../util/InharmonicityModel';
import Tuning from '../util/Tuning';
import EqualTemperament from '../util/EqualTemperament';

import AudioHelper from '../util/AudioHelper';
import Tuners from '../containers/Tuners';
import Keyboard from '../containers/Keyboard';

import './PianoTuner.css';

export interface Props {
  audioHelper: AudioHelper;
  samples: Float32Array;
  selectedKey: KeyName;
  model: InharmonicityModel;
  tuning: Tuning;
  interval: number;
  listening: boolean;
  playing: boolean;
  setSelectedKey: (keyName: string) => {};
  tuneKey: (keyName: string, cents: number) => {};
}

/**
 * Our "Piano Tuner" display.
 */
class PianoTuner extends React.Component<Props> {
  /**
   * Our bandpass filter
   */
  private bandpassFilter = new DSP.Biquad(
    DSP.BPF_CONSTANT_PEAK,
    this.props.audioHelper.sampleRate
  ); // tslint:disable-line

  /**
   * How many samples we've had since we've started listening.
   * NOTE: this will roll over after about 13.5hr of 44.1kHz samples.
   */
  private totalSamples: number = 0;

  /**
   * Our svg drawing area for D3
   */
  private svgElement: SVGSVGElement;

  /**
   * Used for telling us the BPS of the last M3 interval. Useful
   * for helping create the skeleton.
   */
  private lastBps: number = 0;

  constructor(props: Props) {
    super(props);
    this.bandpassFilter.setQ(10);
    this.bandpassFilter.setF0(this.props.tuning[this.props.selectedKey]);
    this.processInterval = this.processInterval.bind(this);
    this.keyClicked = this.keyClicked.bind(this);
  }

  /**
   * Computes information about the given common partial between the
   * low and high keys provided. The index values are indeces into
   * KEY_NAMES whereas the cp values are indeces into PARTIAL_STEPS.
   * @param lowIndex
   * @param lowCp
   * @param highIndex
   * @param highCp
   * @returns a <tr>
   */
  processCp(
    lowIndex: number,
    lowCp: number,
    highIndex: number,
    highCp: number
  ) {
    const { model, tuning } = this.props;
    const cpIndex = lowIndex + PARTIAL_STEPS[lowCp];
    const bps = model.bps(tuning, lowIndex, lowCp - 1, highIndex, highCp - 1);
    const beats = bps.toFixed(2);
    const intervalName = INTERVAL_NAMES[highIndex - lowIndex];
    const lowKey = KEY_NAMES[lowIndex];
    const highKey = KEY_NAMES[highIndex];
    const cpKey = KEY_NAMES[cpIndex];
    let lowHz;
    try {
      lowHz = ` (${tuning[lowKey].toFixed(2)}Hz)`;
    } catch {
      lowHz = '';
    }
    let highHz;
    try {
      highHz = ` (${tuning[highKey].toFixed(2)}Hz)`;
    } catch {
      highHz = '';
    }

    if (intervalName === 'M3') {
      console.log(`Difference from last: ${(bps - this.lastBps).toFixed(2)}`);
      this.lastBps = bps;
    }

    return (
      <tr key={lowKey + highKey + cpKey} className="interval-row">
        <td className="interval-name">{intervalName}</td>
        <td className="interval-partials">
          {lowCp.toFixed(0)}:{highCp.toFixed(0)}
        </td>
        <td className="interval-details">
          {lowKey}
          {lowHz} - {highKey}
          {highHz}
        </td>
        <td className="interval-cp">{cpKey}</td>
        <td className="interval-beats">{beats}bps</td>
      </tr>
    );
  }

  /**
   * Computes information about the common partials for the given
   * interval. The index values are indeces into KEY_NAMES.
   * @param lowIndex
   * @param highIndex
   * @return an array of <tr>
   */
  processCps(lowIndex: number, highIndex: number) {
    const cps = COMMON_PARTIALS[highIndex - lowIndex];
    if (cps) {
      return cps.map((cp: number[]) =>
        this.processCp(lowIndex, cp[0], highIndex, cp[1])
      );
    }
  }

  /**
   * Computes common partial and check note information for the
   * interval indicated by our piano keyboard values from the the
   * store.
   * @returns a <div> holding two tables.
   */
  processInterval() {
    const { tuning, model, selectedKey, interval } = this.props;
    const selectedKeyIndex = KEY_NAMES.indexOf(selectedKey);
    const intervalKeyIndex = selectedKeyIndex + interval;
    const lowIndex = Math.min(selectedKeyIndex, intervalKeyIndex);
    const highIndex = Math.max(selectedKeyIndex, intervalKeyIndex);
    const d = model.totalDissonance(tuning, lowIndex, highIndex);
    let checkKeyIndex;
    switch (Math.abs(interval)) {
      case 5: // Major 4th
        checkKeyIndex = lowIndex - 4;
        break;
      case 7: // Major 5th
        checkKeyIndex = lowIndex - 9;
        break;
      case 12: // Octave
        checkKeyIndex = lowIndex - 4;
        break;
      default:
        checkKeyIndex = 0;
    }

    return (
      <div>
        {lowIndex !== highIndex && (
          <div id="dissonance">Dissonance: {d.toFixed(10)}</div>
        )}
        {COMMON_PARTIALS[highIndex - lowIndex] && (
          <div id="beats-table-left">
            <h2>Common Partial(s)</h2>
            <table>
              <tbody>{this.processCps(lowIndex, highIndex)}</tbody>
            </table>
          </div>
        )}
        {checkKeyIndex > 0 && (
          <div id="beats-table-right">
            <h2>Check Notes</h2>
            <table>
              <tbody>
                {this.processCps(checkKeyIndex, lowIndex)}
                {this.processCps(checkKeyIndex, highIndex)}
              </tbody>
            </table>
          </div>
        )}
      </div>
    );
  }

  /**
   * Draws the filtered signal on top of the last 10 full cycles of our
   * target frequency.
   * @param filtered
   */
  drawStrobe(filtered: Float32Array) {
    while (this.svgElement && this.svgElement.firstChild) {
      this.svgElement.removeChild(this.svgElement.firstChild);
    }

    const numBeatsToShow = 4;
    const samplesPerPeak =
      this.props.audioHelper.sampleRate /
      this.props.tuning[this.props.selectedKey];

    const beatsShown = this.totalSamples / samplesPerPeak;
    const offset = Math.floor(
      this.totalSamples -
        samplesPerPeak * (Math.floor(beatsShown) - numBeatsToShow)
    );
    const start = filtered.length - offset;
    const end = start + Math.floor(samplesPerPeak * numBeatsToShow);

    // We want to provide as tall of a signal as possible,
    // regardless of how loud the input is. We find the max over the
    // signal we're going to plot and use that as our domain for D3.
    let max = 0;
    for (let i = start; i < end; i += 1) {
      max = Math.max(Math.abs(filtered[i]), max);
    }

    draw.drawStrobe(this.svgElement, filtered.slice(start, end + 1), [
      end - start,
      0
    ]);
  }

  /**
   * Updates the bandpass filter F0 whenever the tuning or selected
   * key changes.
   * @param nextProps
   */
  componentWillUpdate(nextProps: Props) {
    const { tuning } = this.props;
    if (tuning !== nextProps.tuning) {
      this.bandpassFilter.setF0(nextProps.tuning[nextProps.selectedKey]);
    }
  }

  componentDidUpdate(prevProps: Props) {
    const { samples } = this.props;
    if (samples !== prevProps.samples) {
      this.totalSamples += samples.length;
      // if (this.props.playing) {
      //   this.props.audioHelper.play(samples);
      // }
      this.drawStrobe(this.bandpassFilter.process(samples));
    }
  }

  /**
   * Handle clicking on a key on the keyboard
   * @param keyName
   */
  keyClicked(keyName: KeyName) {
    const { model, tuning, playing, audioHelper } = this.props;
    // Force focus on the slider associated with the selectedKey
    const element = document.getElementById(`${keyName}-slider`);
    this.bandpassFilter.setF0(tuning[keyName]);
    if (element) {
      (element.children[0].children[3] as HTMLElement).focus();
    }
    if (playing) {
      audioHelper.play(model.samples(keyName, tuning, audioHelper.sampleRate));
    }
  }

  /**
   * Downloads the tuning as tuning.json.
   * @param tuning the tuning
   */
  download(tuning: Tuning) {
    const text = JSON.stringify(tuning, null, '  ');
    const pom = document.createElement('a');
    pom.setAttribute(
      'href',
      'data:text/plain;charset=utf-8,' + encodeURIComponent(text)
    );
    pom.setAttribute('download', 'tuning.json');

    if (document.createEvent) {
      const event = document.createEvent('MouseEvents');
      event.initEvent('click', true, true);
      pom.dispatchEvent(event);
    } else {
      pom.click();
    }
  }

  render() {
    const kac = EqualTemperament.keyAndCents(this.bandpassFilter.f0);
    const targetInfo = `Target: ${this.bandpassFilter.f0.toFixed(2)}Hz (${
      kac.keyName
    } ${kac.cents > 0 ? '+' : ''}${kac.cents.toFixed(2)}c)`;

    return (
      <main id="piano-tuner-page">
        <div className="container">
          <div id="tuning-target">{targetInfo}</div>
          <div id="signal-wrapper">
            <svg
              id="filtered-signal"
              ref={(ref: SVGSVGElement) => (this.svgElement = ref)}
            />
          </div>
          <Tuners />
          <Keyboard showInterval={true} keyClicked={this.keyClicked} />
          {this.processInterval()}
        </div>
      </main>
    );
  }
}

export default PianoTuner;
