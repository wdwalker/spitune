import * as React from 'react';

import AudioHelper from '../util/AudioHelper';

interface Props {
  audioHelper: AudioHelper;
  snippet: Float32Array;
}

class PlayAudio extends React.Component<Props> {
  render() {
    const { snippet } = this.props;
    return (
      <button
        title="Play the current audio snippet"
        id="play-button"
        type="button"
        className="btn"
        disabled={!(snippet && snippet.length > 0)}
        onClick={(e) => {
          if (snippet && snippet.length > 0) {
            this.props.audioHelper.play(snippet);
          }
        }}
      >
        <span className="fa fa-play" />
      </button>
    );
  }
}

export default PlayAudio;
