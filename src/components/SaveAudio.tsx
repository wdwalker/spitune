import * as React from 'react';

import { KeyName } from '../util/Piano';
import AudioHelper from '../util/AudioHelper';

interface Props {
  audioHelper: AudioHelper;
  snippet: Float32Array;
  selectedKey: KeyName;
}

class SaveAudio extends React.Component<Props> {
  save(keyName: KeyName, samples: Float32Array) {
    if (!samples || samples.length < 1) {
      return;
    }
    const wav = this.props.audioHelper.toWav(samples);
    const blob = new window.Blob([new DataView(wav)], {
      type: 'audio/wav'
    });
    const anchor = document.createElement('a');
    const url = window.URL.createObjectURL(blob);
    anchor.href = url;
    anchor.download = `${keyName}.wav`;
    anchor.click();
    window.URL.revokeObjectURL(url);
  }

  render() {
    const { snippet, selectedKey } = this.props;
    return (
      <button
        title="Download the current audio snippet"
        id="download-button"
        type="button"
        className="btn"
        onClick={(e) => {
          this.save(selectedKey, snippet);
        }}
      >
        <span className="fa fa-music" /> <span className="fa fa-download" />
      </button>
    );
  }
}

export default SaveAudio;
