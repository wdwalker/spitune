import * as React from 'react';

import InharmonicityModel from '../util/InharmonicityModel';

interface Props {
  model: InharmonicityModel;
}

class SaveModel extends React.Component<Props> {
  save(model: InharmonicityModel) {
    const text = JSON.stringify(model);
    const pom = document.createElement('a');
    pom.setAttribute(
      'href',
      'data:text/plain;charset=utf-8,' + encodeURIComponent(text)
    );
    pom.setAttribute('download', 'model.json');

    if (document.createEvent) {
      const event = document.createEvent('MouseEvents');
      event.initEvent('click', true, true);
      pom.dispatchEvent(event);
    } else {
      pom.click();
    }
  }

  render() {
    return (
      <button
        title="Download model.json"
        className="btn btn-primary"
        id="save-button"
        onClick={() => this.save(this.props.model)}
      >
        Save Model
      </button>
    );
  }
}

export default SaveModel;
