import * as React from 'react';

import Tuning from '../util/Tuning';

interface Props {
  tuning: Tuning;
}

class SaveTuning extends React.Component<Props> {
  save(tuning: Tuning) {
    const text = JSON.stringify(tuning);
    const pom = document.createElement('a');
    pom.setAttribute(
      'href',
      'data:text/plain;charset=utf-8,' + encodeURIComponent(text)
    );
    pom.setAttribute('download', 'tuning.json');

    if (document.createEvent) {
      const event = document.createEvent('MouseEvents');
      event.initEvent('click', true, true);
      pom.dispatchEvent(event);
    } else {
      pom.click();
    }
  }

  render() {
    return (
      <button
        title="Download tuning.json"
        className="btn btn-primary"
        id="save-button"
        onClick={() => this.save(this.props.tuning)}
      >
        Save Tuning
      </button>
    );
  }
}

export default SaveTuning;
