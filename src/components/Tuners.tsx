import * as React from 'react';
import Slider, { createSliderWithTooltip } from 'rc-slider';
import 'rc-slider/assets/index.css';

const SliderWithTooltip = createSliderWithTooltip(Slider);

import { KeyName, KEY_NAMES } from '../util/Piano';
import Tuning from '../util/Tuning';
import EqualTemperament from '../util/EqualTemperament';

import './Tuners.css';

export interface Props {
  tuning: Tuning;
  tuneKey: (keyName: KeyName, cents: number) => {};
}

/**
 * Our set of sliders that we use for tuning the piano.
 */
class Tuners extends React.Component<Props> {
  li: {} = {};

  constructor(props: Props) {
    super(props);
    this.createTuner = this.createTuner.bind(this);
  }

  createTuner(keyName: KeyName) {
    let cents = 0;
    if (this.props.tuning[keyName]) {
      const keyAndCents = EqualTemperament.keyAndCents(
        this.props.tuning[keyName]
      );
      cents = keyAndCents.cents;
    }
    return (
      <li
        key={`${keyName}-slider`}
        id={`${keyName}-slider`}
        className="tuner-div"
        ref={(ref: HTMLLIElement) => {
          this.li[keyName] = ref;
        }}
      >
        <SliderWithTooltip
          tipFormatter={(v: number) => `${keyName} ${v}c`}
          className="cents-slider"
          vertical={true}
          min={-49}
          max={49}
          step={0.125}
          onChange={(value: number) => this.props.tuneKey(keyName, value)}
          value={cents}
        />
      </li>
    );
  }

  componentDidMount() {
    for (let i = 0; i < KEY_NAMES.length; i += 1) {
      // GOD AWFUL WRESTLING with rc-slider and focus.
      const slider: HTMLElement = this.li[KEY_NAMES[i]].children[0].children[3];
      slider.onblur = (e: FocusEvent) => {
        const previous = e.target as HTMLElement;
        const current = e.relatedTarget as HTMLElement;
        if (
          previous &&
          previous.getAttribute('role') === 'slider' &&
          previous.parentNode
        ) {
          (previous.parentNode as HTMLElement).children[1].classList.remove(
            'highlight'
          );
        }
        if (
          current &&
          current.getAttribute('role') === 'slider' &&
          current.parentNode
        ) {
          (current.parentNode as HTMLElement).children[1].classList.add(
            'highlight'
          );
        }
      };
    }
  }

  render() {
    return (
      <div id="tuners-wrapper">
        <ul id="tuners">{KEY_NAMES.map(this.createTuner)}</ul>
      </div>
    );
  }
}

export default Tuners;
