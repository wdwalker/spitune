import * as React from 'react';

import AudioHelper from '../util/AudioHelper';
import { KeyName, KEY_NAMES } from '../util/Piano';

import './UploadAudio.css';

interface Props {
  audioHelper: AudioHelper;
  setSnippet: (snippet: Float32Array) => {};
  setSelectedKey: (keyName: string) => {};
}

class UploadAudio extends React.Component<Props> {
  constructor(props: Props) {
    super(props);
    this.loadFile = this.loadFile.bind(this);
  }

  loadFile(e: React.ChangeEvent<HTMLInputElement>) {
    if (!e.target || !e.target.files) {
      return;
    }
    const file = e.target.files[0];
    const keyName: KeyName = file.name.split('.')[0] as KeyName;
    if (KEY_NAMES.indexOf(keyName) >= 0) {
      this.props.setSelectedKey(keyName);
    }
    setTimeout(
      this.props.audioHelper.toSamples(file, (samples: Float32Array) => {
        this.props.setSnippet(samples);
      }),
      20
    );
  }

  render() {
    return (
      <label
        id="upload-button"
        title="Upload an audio snippet"
        className="btn btn-file"
      >
        <span className="fa fa-music" /> <span className="fa fa-upload" />
        <input type="file" onChange={this.loadFile} />
      </label>
    );
  }
}

export default UploadAudio;
