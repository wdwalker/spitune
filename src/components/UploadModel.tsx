import * as React from 'react';

import InharmonicityModel from '../util/InharmonicityModel';

import './UploadTuning.css';

interface Props {
  model: InharmonicityModel;
  setModel: (model: InharmonicityModel) => {};
}

class UploadModel extends React.Component<Props> {
  constructor(props: Props) {
    super(props);
    this.loadFile = this.loadFile.bind(this);
  }

  toModel(file: File, cb: (model: InharmonicityModel) => void) {
    const reader = new FileReader();
    reader.onload = () => {
      const raw = JSON.parse(reader.result);
      cb(new InharmonicityModel(raw));
    };
    reader.readAsText(file);
  }

  loadFile(e: React.ChangeEvent<HTMLInputElement>) {
    if (!e.target || !e.target.files || !e.target.files.length) {
      return;
    }
    const file = e.target.files[0];
    setTimeout(
      this.toModel(file, (model: InharmonicityModel) =>
        this.props.setModel(model)
      ),
      20
    );
  }

  render() {
    return (
      <label id="upload-button" title="Upload a model" className="btn btn-file">
        Upload Model
        <input type="file" onChange={this.loadFile} />
      </label>
    );
  }
}

export default UploadModel;
