import * as React from 'react';

import { KeyName } from '../util/Piano';
import Tuning from '../util/Tuning';

import './UploadTuning.css';

interface Props {
  tuning: Tuning;
  setTuning: (tuning: Tuning) => {};
}

class UploadAudio extends React.Component<Props> {
  constructor(props: Props) {
    super(props);
    this.loadFile = this.loadFile.bind(this);
  }

  toTuning(file: File, cb: (tuning: Tuning) => void) {
    const reader = new FileReader();
    const tuning: Tuning = new Map<KeyName, number>();
    reader.onload = () => {
      const raw = JSON.parse(reader.result);
      for (const key in raw) {
        if (key) {
          tuning[key as KeyName] = raw[key];
        }
      }
      cb(tuning);
    };
    reader.readAsText(file);
  }

  loadFile(e: React.ChangeEvent<HTMLInputElement>) {
    if (!e.target || !e.target.files || !e.target.files.length) {
      return;
    }
    const file = e.target.files[0];
    setTimeout(
      this.toTuning(file, (tuning: Tuning) => this.props.setTuning(tuning)),
      20
    );
  }

  render() {
    return (
      <label
        id="upload-button"
        title="Upload a tuning"
        className="btn btn-file"
      >
        Upload Tuning
        <input type="file" onChange={this.loadFile} />
      </label>
    );
  }
}

export default UploadAudio;
