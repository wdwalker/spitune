import { connect, Dispatch } from 'react-redux';
import { withRouter } from 'react-router-dom';

import * as actions from '../actions/';
import { StoreState } from '../store';
import App from '../components/App';

const mapStateToProps = ({ processing }: StoreState) => {
  return { processing };
};
const mapDispatchToProps = (dispatch: Dispatch<actions.ActionTypes>) => {
  return {};
};

// https://github.com/DefinitelyTyped/DefinitelyTyped/issues/18999#issuecomment-368868812
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(
  App as any // tslint:disable-line
) as any); // tslint:disable-line
