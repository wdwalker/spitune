import { connect, Dispatch } from 'react-redux';

import * as actions from '../actions/';
import { StoreState } from '../store';
import AudioController from '../components/AudioController';

const mapStateToProps = ({
  audioHelper,
  listening,
  playing,
  active
}: StoreState) => {
  return {
    audioHelper,
    listening,
    playing,
    active
  };
};

const mapDispatchToProps = (dispatch: Dispatch<actions.ActionTypes>) => {
  return {
    newSamples: (samples: Float32Array) =>
      dispatch(actions.newSamples(samples)),
    setListening: (value: boolean) => dispatch(actions.setListening(value)),
    setPlaying: (value: boolean) => dispatch(actions.setPlaying(value)),
    setActive: (value: boolean) => dispatch(actions.setActive(value))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(
  AudioController as any // tslint:disable-line
);
