import { connect, Dispatch } from 'react-redux';

import * as actions from '../actions/';
import { StoreState } from '../store';
import { KeyName } from '../util/Piano';
import BeatFilter from '../components/BeatFilter';

const mapStateToProps = ({
  audioHelper,
  samples,
  playing,
  active,
  selectedKey
}: StoreState) => {
  return {
    audioHelper,
    samples,
    playing,
    active,
    selectedKey
  };
};

const mapDispatchToProps = (dispatch: Dispatch<actions.ActionTypes>) => {
  return {
    setSelectedKey: (keyName: KeyName) =>
      dispatch(actions.setSelectedKey(keyName))
  };
};

// tslint:disable-next-line
export default connect(mapStateToProps, mapDispatchToProps)(BeatFilter as any);
