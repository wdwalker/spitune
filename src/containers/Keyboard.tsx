import { connect, Dispatch } from 'react-redux';

import * as actions from '../actions/';
import { StoreState } from '../store';
import { KeyName } from '../util/Piano';
import Keyboard from '../components/Keyboard';

interface OwnProps {
  showInterval?: boolean;
  keyClicked?: (keyName: KeyName) => void;
}

const mapStateToProps = (
  { selectedKey, interval }: StoreState,
  ownProps: OwnProps
) => {
  return {
    selectedKey,
    interval
  };
};

const mapDispatchToProps = (dispatch: Dispatch<actions.ActionTypes>) => {
  return {
    setSelectedKey: (keyName: KeyName) =>
      dispatch(actions.setSelectedKey(keyName))
  };
};

// tslint:disable-next-line
export default connect(mapStateToProps, mapDispatchToProps)(Keyboard as any);
