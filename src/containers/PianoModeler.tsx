import { connect, Dispatch } from 'react-redux';

import * as actions from '../actions/';
import { StoreState } from '../store';
import { KeyName } from '../util/Piano';
import PianoModeler from '../components/PianoModeler';

const mapStateToProps = ({
  samples,
  snippet,
  model,
  coarseFFT,
  preciseFFT,
  audioHelper,
  playing,
  active,
  selectedKey
}: StoreState) => {
  return {
    samples,
    snippet,
    model,
    coarseFFT,
    preciseFFT,
    audioHelper,
    playing,
    active,
    selectedKey
  };
};

const mapDispatchToProps = (dispatch: Dispatch<actions.ActionTypes>) => {
  return {
    setSnippet: (snippet: Float32Array) =>
      dispatch(actions.setSnippet(snippet)),
    setProcessing: (value: boolean) => dispatch(actions.setProcessing(value)),
    setSelectedKey: (keyName: KeyName) =>
      dispatch(actions.setSelectedKey(keyName))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(
  PianoModeler as any // tslint:disable-line
);
