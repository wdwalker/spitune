import { connect, Dispatch } from 'react-redux';

import * as actions from '../actions/';
import { StoreState } from '../store';
import { KeyName } from '../util/Piano';
import PianoTuner from '../components/PianoTuner';

const mapStateToProps = ({
  audioHelper,
  samples,
  selectedKey,
  model,
  tuning,
  interval,
  listening,
  playing
}: StoreState) => {
  return {
    audioHelper,
    samples,
    selectedKey,
    model,
    tuning,
    interval,
    listening,
    playing
  };
};

const mapDispatchToProps = (dispatch: Dispatch<actions.ActionTypes>) => {
  return {
    setSelectedKey: (keyName: KeyName) =>
      dispatch(actions.setSelectedKey(keyName)),
    tuneKey: (keyName: KeyName, cents: number) =>
      dispatch(actions.tuneKey(keyName, cents))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(
  PianoTuner as any // tslint:disable-line
);
