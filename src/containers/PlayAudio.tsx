import { connect, Dispatch } from 'react-redux';

import * as actions from '../actions/';
import { StoreState } from '../store';
import PlayAudio from '../components/PlayAudio';

const mapStateToProps = ({ audioHelper, snippet }: StoreState) => {
  return {
    audioHelper,
    snippet
  };
};

const mapDispatchToProps = (dispatch: Dispatch<actions.ActionTypes>) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(
  PlayAudio as any // tslint:disable-line
);
