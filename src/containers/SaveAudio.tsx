import { connect, Dispatch } from 'react-redux';

import * as actions from '../actions/';
import { StoreState } from '../store';
import SaveAudio from '../components/SaveAudio';

const mapStateToProps = ({ audioHelper, snippet, selectedKey }: StoreState) => {
  return {
    audioHelper,
    snippet,
    selectedKey
  };
};

const mapDispatchToProps = (dispatch: Dispatch<actions.ActionTypes>) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(
  SaveAudio as any // tslint:disable-line
);
