import { connect, Dispatch } from 'react-redux';

import * as actions from '../actions/';
import { StoreState } from '../store';
import SaveModel from '../components/SaveModel';

const mapStateToProps = ({ model }: StoreState) => {
  return {
    model
  };
};

const mapDispatchToProps = (dispatch: Dispatch<actions.ActionTypes>) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(
  SaveModel as any // tslint:disable-line
);
