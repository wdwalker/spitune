import { connect, Dispatch } from 'react-redux';

import * as actions from '../actions/';
import { StoreState } from '../store';
import SaveTuning from '../components/SaveTuning';

const mapStateToProps = ({ tuning }: StoreState) => {
  return {
    tuning
  };
};

const mapDispatchToProps = (dispatch: Dispatch<actions.ActionTypes>) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(
  SaveTuning as any // tslint:disable-line
);
