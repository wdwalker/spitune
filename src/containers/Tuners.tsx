import { connect, Dispatch } from 'react-redux';

import * as actions from '../actions/';
import { StoreState } from '../store';
import { KeyName } from '../util/Piano';
import Tuners from '../components/Tuners';

const mapStateToProps = ({ tuning }: StoreState) => {
  return { tuning };
};

const mapDispatchToProps = (dispatch: Dispatch<actions.ActionTypes>) => {
  return {
    tuneKey: (keyName: KeyName, cents: number) =>
      dispatch(actions.tuneKey(keyName, cents))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(
  Tuners as any // tslint:disable-line
);
