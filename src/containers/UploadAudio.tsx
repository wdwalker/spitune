import { connect, Dispatch } from 'react-redux';

import * as actions from '../actions/';
import { StoreState } from '../store';
import { KeyName } from '../util/Piano';
import UploadAudio from '../components/UploadAudio';

const mapStateToProps = ({ audioHelper }: StoreState) => {
  return {
    audioHelper
  };
};

const mapDispatchToProps = (dispatch: Dispatch<actions.ActionTypes>) => {
  return {
    setSnippet: (snippet: Float32Array) =>
      dispatch(actions.setSnippet(snippet)),
    setSelectedKey: (keyName: KeyName) =>
      dispatch(actions.setSelectedKey(keyName))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(
  UploadAudio as any // tslint:disable-line
);
