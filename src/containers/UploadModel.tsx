import { connect, Dispatch } from 'react-redux';

import * as actions from '../actions/';
import { StoreState } from '../store';
import InharmonicityModel from '../util/InharmonicityModel';
import UploadModel from '../components/UploadModel';

const mapStateToProps = ({ model }: StoreState) => {
  return {
    model
  };
};

const mapDispatchToProps = (dispatch: Dispatch<actions.ActionTypes>) => {
  return {
    setModel: (model: InharmonicityModel) => dispatch(actions.setModel(model))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(
  UploadModel as any // tslint:disable-line
);
