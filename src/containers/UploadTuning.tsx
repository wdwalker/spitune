import { connect, Dispatch } from 'react-redux';

import * as actions from '../actions/';
import { StoreState } from '../store';
import Tuning from '../util/Tuning';
import UploadTuning from '../components/UploadTuning';

const mapStateToProps = ({ tuning }: StoreState) => {
  return {
    tuning
  };
};

const mapDispatchToProps = (dispatch: Dispatch<actions.ActionTypes>) => {
  return {
    setTuning: (tuning: Tuning) => dispatch(actions.setTuning(tuning))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(
  UploadTuning as any // tslint:disable-line
);
