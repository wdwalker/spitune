import { ActionKeys, ActionTypes } from '../actions';
import { StoreState } from '../store';
import { KEY_NAMES, KeyName } from '../util/Piano';
import Tuning from '../util/Tuning';
import EqualTemperament from '../util/EqualTemperament';

function reducer(state: StoreState, action: ActionTypes): StoreState {
  switch (action.type) {
    case ActionKeys.NEW_SAMPLES:
      return { ...state, samples: action.samples };
    case ActionKeys.SET_SNIPPET:
      return { ...state, snippet: action.snippet };
    case ActionKeys.SET_LISTENING:
      return { ...state, listening: action.value };
    case ActionKeys.SET_PLAYING:
      return { ...state, playing: action.value };
    case ActionKeys.SET_ACTIVE:
      return { ...state, active: action.value };
    case ActionKeys.SET_PROCESSING:
      return { ...state, processing: action.value };
    case ActionKeys.SET_SELECTED_KEY:
      const interval = state
        ? KEY_NAMES.indexOf(state.selectedKey) -
          KEY_NAMES.indexOf(action.keyName)
        : 0;
      return { ...state, interval, selectedKey: action.keyName };
    case ActionKeys.SET_MODEL:
      return { ...state, model: action.model };
    case ActionKeys.SET_TUNING:
      return { ...state, tuning: action.tuning };
    case ActionKeys.TUNE_KEY: {
      const tuning: Tuning = new Map<KeyName, number>();
      for (const keyName in state.tuning) {
        if (keyName) {
          tuning[keyName] = state.tuning[keyName];
        }
      }
      const hz = EqualTemperament.hz(action.keyName, action.cents);
      tuning[action.keyName] = hz;
      return { ...state, tuning };
    }
    default:
      return state;
  }
}

export default reducer;
