import { createStore, StoreCreator } from 'redux';
import reducer from './reducers/index';

import AudioHelper from './util/AudioHelper';
import { KeyName } from './util/Piano';
import Tuning from './util/Tuning';
import InharmonicityModel from './util/InharmonicityModel';
import FFT from './util/FFT';

import C3Model from './C3model';
const model: InharmonicityModel = new InharmonicityModel(C3Model);

import C3Tuning from './C3Tuning';
const tuning: Tuning = new Map<KeyName, number>();
for (const key in C3Tuning) {
  if (key) {
    tuning[key as KeyName] = C3Tuning[key];
  }
}

// We compute the number of FFT points as a power of 2, with the
// lowest number of points being 2^16 (65536).
//
// The following ms are from a 2.6 GHz Intel Quad Core i7:
//   +/- 0.02103Hz takes ~800ms with gaps in bandpass playback
//   +/- 0.04206Hz takes ~320ms with gaps in bandpass playback
//   +/- 0.08411Hz takes ~100ms with no gaps in bandpass playback
const DESIRED_COARSE_HZ_PRECISION = 0.1;
const DESIRED_PRECISE_HZ_PRECISION = 0.02;
const SAMPLE_RATE = 44100;

// Set up our coarse FFT, which we will use to plot our signal
// while we are within our listening endpoint
let coarseFFTSize = 65536;
while (SAMPLE_RATE / coarseFFTSize > DESIRED_COARSE_HZ_PRECISION) {
  coarseFFTSize *= 2;
}

// Set up our precise FFT, which we will use to plot our signal
// once we dip below our threshold (store.stopRMS)
let preciseFFTSize = coarseFFTSize;
while (SAMPLE_RATE / preciseFFTSize > DESIRED_PRECISE_HZ_PRECISION) {
  preciseFFTSize *= 2;
}

export interface StoreState {
  audioHelper: AudioHelper;
  samples: Float32Array;
  snippet: Float32Array;
  model: InharmonicityModel;
  tuning: Tuning;
  coarseFFT: FFT;
  preciseFFT: FFT;
  listening: boolean;
  playing: boolean;
  active: boolean;
  processing: boolean;
  selectedKey: KeyName;
  interval: number;
}

// tslint:disable
// WDW: got rid of DEVTOOL because it was slowing things waaaaaay down.
// Need to investigate into what is going on.
const enhancer: StoreCreator = window['__REDUUUUUX_DEVTOOLS_EXTENSION__']
  ? window['__REDUX_DEVTOOLS_EXTENSION__']()(createStore)
  : createStore;
// tslint:enable

const coarseFFT = new FFT(coarseFFTSize, SAMPLE_RATE);
const preciseFFT = new FFT(preciseFFTSize, SAMPLE_RATE);
const store = enhancer<StoreState>(reducer, {
  model,
  tuning,
  coarseFFT,
  preciseFFT,
  samples: new Float32Array(0),
  snippet: new Float32Array(0),
  audioHelper: new AudioHelper(44100, 16384),
  listening: false,
  playing: false,
  active: false,
  processing: false,
  selectedKey: 'A4',
  interval: 0
});

export default store;
