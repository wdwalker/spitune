import DSP from 'digitalsignals';
import bufferToWav from 'audiobuffer-to-wav';

// We need to do this or TypeScript complains.
declare namespace window {
  const AudioContext: {
    new (): AudioContext;
  };
  const webkitAudioContext: {
    new (): AudioContext;
  };
}

const AudioContext = window.AudioContext || window.webkitAudioContext || false;

export type Callback = (samples: Float32Array) => void;

/**
 * Helper interface to WebAudio.
 */
class AudioHelper {
  public readonly sampleRate: number;
  public active: boolean = false;
  private bufferSize: number;
  private audioContext: any; // tslint:disable-line
  private stream: MediaStream;
  private audioNode: MediaStreamAudioSourceNode;
  private recorder: ScriptProcessorNode;
  private callback: Callback;
  private averageQuietRMS: number = 0.0005;

  /**
   * Create a new AudioHelper
   * @param  {number} sampleRate the sample rate we want
   * @param  {number} bufferSize how big of a recording buffer we want
   * @return {AudioHelper} the new AudioHelper
   */
  constructor(sampleRate: number, bufferSize: number) {
    if (!AudioContext) {
      alert(
        'This app will not work because WebAudio does not work in this browser.'
      );
      return;
    }
    this.audioContext = new AudioContext();
    this.sampleRate = sampleRate;
    this.bufferSize = bufferSize;
    this.onAudio = this.onAudio.bind(this);
  }

  /**
   * [startRecording description]
   * @param  {func}   onAudio function to call when we get audio input
   * @return {undefined}
   */
  startRecording(callback: (samples: Float32Array) => void) {
    navigator.mediaDevices
      .getUserMedia({
        audio: true,
        video: false
      })
      .then((stream) => {
        this.stream = stream;
        this.audioNode = this.audioContext.createMediaStreamSource(this.stream);
        this.recorder = this.audioContext.createScriptProcessor(
          this.bufferSize,
          1,
          1
        );
        this.callback = callback;
        this.recorder.onaudioprocess = this.onAudio;
        this.audioNode.connect(this.recorder);
        this.recorder.connect(this.audioContext.destination);
      })
      .catch((e) => {
        alert(e);
      });
  }

  /**
   * Stops recording.
   * @return {undefined}
   */
  stopRecording() {
    this.recorder.disconnect();
    this.audioNode.disconnect();
    this.stream.getTracks()[0].stop();
  }

  /**
   * Plays the given samples
   * @param  {Float32Array} samples samples to play
   * @return {undefined}
   */
  play(samples: Float32Array) {
    const outputBuffer = this.audioContext.createBuffer(
      1,
      samples.length,
      this.sampleRate
    );
    const source = this.audioContext.createBufferSource();
    const buffer = outputBuffer.getChannelData(0);
    for (let i = 0; i < samples.length; i += 1) {
      buffer[i] = samples[i];
    }
    source.buffer = outputBuffer;
    source.connect(this.audioContext.destination);
    source.start();
  }

  /**
   * Converts the given samples to a WAV buffer
   * @param samples
   */
  toWav(samples: Float32Array) {
    const buffer = this.audioContext.createBuffer(
      1,
      samples.length,
      this.sampleRate
    );
    buffer.copyToChannel(samples, 0);
    return bufferToWav(buffer);
  }

  /**
   * Reads in the WAV file as a Float32Array of samples
   * @param file
   * @param cb
   */
  toSamples(file: File, cb: (samples: Float32Array) => void) {
    const reader = new FileReader();
    reader.onload = () => {
      const rawData = reader.result;
      this.audioContext.decodeAudioData(rawData, (buffer: AudioBuffer) => {
        cb(buffer.getChannelData(0));
      });
    };
    reader.readAsArrayBuffer(file);
  }

  /**
   * Trims 'silence' from the beginning of samples.
   * @param samples the buffer of samples
   * @returns the trimmed samples
   */
  trim(samples: Float32Array): Float32Array {
    let start = 0;
    while (start < this.bufferSize) {
      const rms = DSP.RMS(samples.slice(start, start + this.sampleRate / 100));
      if (rms > 10 * this.averageQuietRMS) {
        break;
      }
      start += 1;
    }
    return samples.slice(start);
  }

  // tslint:disable-next-line
  private onAudio(ape: any) {
    const samples: Float32Array = ape.inputBuffer.getChannelData(0);
    const rms = DSP.RMS(samples);

    // 0.707 is the rms for a perfect sine wave. We go active when the
    // rms of our samples exceeds 20% of the distance between the quiet
    // rms and the maximum rms of .707.
    const onLimit = this.averageQuietRMS + (0.707 - this.averageQuietRMS) / 5;

    // We go inactive when the rms dips below double the average quiet rms.
    const offLimit = 2.0 * this.averageQuietRMS;

    if (this.active && rms < offLimit) {
      this.active = false;
    } else if (!this.active && rms > onLimit) {
      this.active = true;
    } else if (!this.active) {
      // We compute our average quiet RMS when we are inactive, and it is
      // done as a rolling average over the past 2 seconds of audio.
      const windowSize = 2 / (samples.length / this.sampleRate);
      this.averageQuietRMS -= this.averageQuietRMS / windowSize;
      this.averageQuietRMS += rms / windowSize;
    }

    this.callback(samples);
  }
}

export default AudioHelper;
