import * as DSP from 'digitalsignals';
import * as draw from './draw';
import * as stats from './stats';

import { KeyName } from './Piano';
import EqualTemperament from './EqualTemperament';

/**
 * Wraps an FFT and holds information around it and its relationship
 * to the frequency range of a piano (e.g., hzPerBin)
 */
class FFT {
  public readonly fft: any; // tslint:disable-line
  public readonly hzPerBin: number;
  private readonly buffer: Float32Array;
  private readonly minBin: number;
  private readonly maxBin: number;

  constructor(fftSize: number, sampleRate: number) {
    this.fft = new DSP.FFT(fftSize, sampleRate);
    this.buffer = new Float32Array(fftSize).fill(0);
    this.hzPerBin = sampleRate / fftSize;
    this.minBin = Math.floor(EqualTemperament.minHz / this.hzPerBin);
    this.maxBin = Math.floor(EqualTemperament.maxHz / this.hzPerBin);
  }

  /**
   * Does a DC bias correction, applies a Hamming window, and then
   * computes the FFT.
   */
  process = (samples: Float32Array | number[]) => {
    let sum = 0.0;
    for (let i = 0; i < samples.length; i += 1) {
      sum += samples[i];
    }
    const dcBias = sum / samples.length;
    for (let i = 0; i < samples.length; i += 1) {
      this.buffer[i] = samples[i] - dcBias;
      this.buffer[i] *= DSP.WindowFunction.Hamming(samples.length, i);
    }
    this.fft.forward(this.buffer);
    return this.buffer.slice(0, samples.length);
  };

  /**
   * Clears the buffer that holds the samples for our FFT.
   */
  reset = () => {
    this.buffer.fill(0);
  };

  /**
   * Finds the peak power in the FFT that is +/- range around the center
   * @param center the center frequency we care about
   * @param range  multiplier (e.g., 0.02 is 98%-102% of center)
   * @returns the peak; if all other peaks are less than 50% of this one,
   *          we return a positive number; otherwise we return negative.
   */
  findPeak(center: number, range: number = 0.02) {
    // Find the peak
    const minBin = Math.floor((1 - range) * center / this.hzPerBin);
    const maxBin = Math.floor((1 + range) * center / this.hzPerBin) + 1;
    let peak = minBin;
    for (let i = minBin; i < maxBin; i += 1) {
      if (this.fft.spectrum[i] >= this.fft.spectrum[peak]) {
        peak = i;
      }
    }

    // Check to see if it is not in a maze of noise
    const cps = stats.criticalPoints(this.fft.spectrum, minBin, maxBin);
    const maxima = cps.maxima.filter(
      (bin: number) => this.fft.spectrum[bin] > this.fft.spectrum[peak] * 0.5
    );

    if (maxima.length === 1) {
      return {
        peakPower: this.fft.spectrum[peak],
        peakHz: peak * this.hzPerBin
      };
    }
    return {
      peakPower: this.fft.spectrum[peak],
      peakHz: -peak * this.hzPerBin
    };
  }

  /**
   * Draws the spectrum on the given svgElement. If keyName is present,
   * this draws only the spectrum for the frequency range of the key,
   * which is +/- 50 cents around the equal temperament value for the key.
   */
  draw = (
    svgElement: SVGSVGElement,
    keyName: KeyName | null = null,
    peakLimit: number = 0.15
  ) => {
    while (svgElement && svgElement.firstChild) {
      svgElement.removeChild(svgElement.firstChild);
    }
    let maximaHz = [];
    if (!keyName) {
      draw.drawGrid(svgElement, 88, 7, 8);
      const slice = this.fft.spectrum.slice(this.minBin, this.maxBin + 1);
      const peak = draw.drawSpectrum(
        svgElement,
        slice,
        EqualTemperament.minHz,
        this.hzPerBin,
        '#00ffff',
        7,
        8
      );
      maximaHz = [(this.minBin + peak) * this.hzPerBin];
      const maxima = [peak];
      draw.drawSpectralMaxima(
        svgElement,
        slice,
        peak,
        maxima,
        EqualTemperament.minHz,
        this.hzPerBin,
        'red',
        7,
        8
      );
    } else {
      draw.drawCents(svgElement);
      const minHz = EqualTemperament.hz(keyName, -50);
      const minBin = Math.floor(minHz / this.hzPerBin);
      const maxHz = EqualTemperament.hz(keyName, +50);
      const maxBin = Math.floor(maxHz / this.hzPerBin);
      const slice = this.fft.spectrum.slice(minBin, maxBin + 1);
      const peak = draw.drawSpectrum(
        svgElement,
        slice,
        minHz,
        this.hzPerBin,
        '#00ffff'
      );
      const cps = stats.criticalPoints(slice, 0, slice.length);
      const maxima = cps.maxima
        .filter((bin: number) => slice[bin] > slice[peak] * peakLimit)
        .sort((a: number, b: number) => slice[b] - slice[a]);
      draw.drawSpectralMaxima(
        svgElement,
        slice,
        peak,
        maxima,
        minHz,
        this.hzPerBin,
        'red'
      );
      for (let i = 0; i < maxima.length; i += 1) {
        maximaHz[i] = (minBin + maxima[i]) * this.hzPerBin;
      }
    }
    return maximaHz;
  };
}

export default FFT;
