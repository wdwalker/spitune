import EqualTemperament from './EqualTemperament';
import FFT from './FFT';
import { KeyName, KEY_NAMES } from './Piano';
import Tuning from './Tuning';

/**
 * Inharmonicity model for a string. This is a set of parallel arrays.
 * The 0th element in each array represents the 1st partial for the string
 * and we go upwards from there.
 */
export type StringModel = {
  /**
   * The measured frequency for the partials. These should all be recorded
   * and determined at once.
   */
  fin: number[];
  /**
   * The measured FFT power for the given partial.
   */
  power: number[];
  /**
   * The calculated inharmonicity for each partial, based upon integer
   * multiples of fin[0] compared to the actual measure fin[n].
   */
  b: number[];
};

/**
 * A model that extends the concept of the inharmonicity to having a unique
 * constant for each partial as opposed to a single constant for all partials.
 */
class InharmonicityModel {
  private model: Map<KeyName, StringModel> = new Map<KeyName, StringModel>();

  // Constants for the dissonance calculations.
  private readonly b1 = 3.5;
  private readonly b2 = 5.75;
  private readonly ss = 0.24;
  private readonly s1 = 0.021;
  private readonly s2 = 19;

  /**
   * Produces a sine wave at the given frequency.
   * @param hZ
   * @param sampleRate
   * @param seconds
   * @param volume
   */
  private static sineWave(
    hZ: number,
    sampleRate: number,
    seconds: number = 3.0,
    volume: number = 1.0
  ) {
    const buffer: Float32Array = new Float32Array(seconds * sampleRate);
    const sampleFreq = sampleRate / hZ;
    for (let i = 0; i < sampleRate * seconds; i += 1) {
      buffer[i] = Math.sin(i / (sampleFreq / (Math.PI * 2))) * volume;
    }
    return buffer;
  }

  /**
   * Create a new InharmonicityModel from the given input.
   * @param model
   */
  constructor(model: { [keyName: string]: StringModel }) {
    for (const key in model) {
      if (key) {
        this.set(key as KeyName, model[key]);
      }
    }
  }

  /**
   * We want just what we need to read this back in.
   */
  toJSON() {
    return this.model;
  }

  /**
   * Compute the inharmonicity constant, b, for the given partial n. This comes from
   * equation 10 in Simon Hendry's masters thesis, "Inharmonicity of Piano Strings"
   * http://www.simonhendry.co.uk/wp/wp-content/uploads/2012/08/inharmonicity.pdf
   * @param  {number} f   the theoretical frequency for partial n
   * @param  {number} fin the measured frequency for partial n
   * @param  {number} n   which partial (n=0 is the first partial)
   * @return {number}     the inharmonicity constant
   */
  b(f: number, fin: number, n: number) {
    return (fin ** 2 - f ** 2) / (f ** 2 * (n + 1) ** 2);
  }

  /**
   * Compute the predicted frequency, fin, based upon the inharmonicity
   * in the model.
   * @param  {KeyName} keyName the key (from KEY_NAMES)
   * @param  {number}  f0      the Hz of the first partial
   * @param  {number}  n       which partial (n=0 is the first partial)
   * @return {number}          the predicted frequency
   */
  fin(keyName: KeyName, f0: number, n: number) {
    if (!this.model[keyName]) {
      return 0;
    }
    const b = this.model[keyName].b[n];
    return f0 * (n + 1) * Math.sqrt(1 + b * (n + 1) ** 2);
  }

  /**
   * Given a target partial n at frequency f, find the first partial (f0) for it.
   * @param  {KeyName} keyName the key (from KEY_NAMES)
   * @param  {number} n        which partial (n=0 is the first partial)
   * @return {number}          what the first partial has to be for partial n to be f
   */
  f0(keyName: KeyName, f: number, n: number) {
    if (!this.model[keyName]) {
      return 0;
    }
    return (
      f / Math.sqrt(1 + this.model[keyName].b[n + 1] * (n + 1) ** 2) / (n + 1)
    );
  }

  /**
   * Calculates the BPS for the given coincidental partial.
   * @param tuning The current tuning
   * @param indexA Index of the first key in KEY_NAMES
   * @param nA     Partial of the first key (0-based)
   * @param indexB Index of the second key in KEY_NAMES
   * @param nB     Partial of the second key (0-based)
   * @returns      BPS - if negative, it's narrow
   */
  bps(
    tuning: Tuning,
    indexA: number,
    nA: number,
    indexB: number,
    nB: number
  ): number {
    const nameA: KeyName = KEY_NAMES[indexA];
    const hzA: number = tuning[nameA];
    const nameB: KeyName = KEY_NAMES[indexB];
    const hzB: number = tuning[nameB];
    const finA = this.fin(nameA, hzA, nA);
    const finB = this.fin(nameB, hzB, nB);
    return finB - finA;
  }

  /**
   * Calculates the dissonance for the given partials.
   */
  d2(finA: number, finB: number): number {
    const s = this.ss / (this.s1 * Math.min(finA, finB) + this.s2);
    const e1 = Math.E ** (-this.b1 * s * Math.abs(finB - finA));
    const e2 = Math.E ** (-this.b2 * s * Math.abs(finB - finA));
    return e1 - e2;
  }

  /**
   * Calculates the dissonance for the given interval. This is based upon
   * N. Giordano's paper, Explaining the Railsback stretch in terms of the
   * inharmonicity of piano tones and sensory dissonance (see
   * https://asa.scitation.org/doi/pdf/10.1121/1.4931439).
   * @param tuning The current tuning
   * @param indexA Index of the first key in KEY_NAMES
   * @param indexB Index of the second key in KEY_NAMES
   * @returns      BPS - if negative, it's narrow
   */
  totalDissonance(tuning: Tuning, indexA: number, indexB: number) {
    const nameA: KeyName = KEY_NAMES[indexA];
    const nameB: KeyName = KEY_NAMES[indexB];
    const hzA: number = tuning[nameA];
    const hzB: number = tuning[nameB];
    let sum = 0;
    for (let nA = 0; nA < this.model[nameA].fin.length; nA += 1) {
      for (let nB = 0; nB < this.model[nameB].fin.length; nB += 1) {
        // const B = this.model[nameA].power[nA] * this.model[nameA].power[nB];
        const B = Math.min(
          this.model[nameA].power[nA],
          this.model[nameA].power[nB]
        );
        const finA = this.fin(nameA, hzA, nA);
        const finB = this.fin(nameB, hzB, nB);
        sum += B * this.d2(finA, finB);
      }
    }
    return sum / 2;
  }

  /**
   * Given the computed FFT for a given string, determine the inharmonicity
   * model for the string.
   * @param keyName The given string
   * @param fft     The computed FFT
   * @param max     The maximum number of partials to find
   */
  generate(keyName: KeyName, fft: FFT, max: number = 8): StringModel {
    // Find our first partial.
    const p0 = fft.findPeak(EqualTemperament.hz(keyName));

    // Now shoot on up each partial. Note that we add the base frequency
    // to the last computed peak as an attempt to somewhat fdollow the
    // inharmonicity.
    const fin = [];
    const b = [];
    const power = [];
    let n = 0;
    let freq = Math.abs(p0.peakHz);
    while (Math.abs(freq) < EqualTemperament.maxHz && n < max) {
      const { peakPower, peakHz } = fft.findPeak(freq);
      fin[n] = peakHz;
      b[n] = this.b(p0.peakHz * (n + 1), fin[n], n);
      power[n] = peakPower;
      freq = Math.abs(peakHz) + Math.abs(p0.peakHz);
      n += 1;
    }
    return { fin, b, power };
  }

  /**
   * Gets the string model for the given key.
   */
  get(keyName: KeyName): StringModel {
    return this.model[keyName];
  }

  /**
   * Sets the string model for the given key.
   */
  set(keyName: KeyName, stringModel: StringModel) {
    this.model[keyName] = stringModel;
  }

  /**
   * Returns an array of audio samples to play for the given key at the
   * given tuning. This brings in sine waves for each of the partials at
   * their relative power levels from the model.
   * @param keyName
   * @param tuning
   * @param sampleRate
   * @param seconds
   */
  samples(
    keyName: KeyName,
    tuning: Tuning,
    sampleRate: number,
    seconds: number = 3.0
  ) {
    const f0 = tuning[keyName];
    if (!this.model[keyName] || this.model[keyName].power.length === 0) {
      return InharmonicityModel.sineWave(f0, sampleRate, seconds, 1.0);
    }
    const buffer = new Float32Array(sampleRate * seconds);
    const power = this.model[keyName].power;
    let sum = 0;
    for (let i = 0; i < power.length; i += 1) {
      sum += power[i];
    }
    buffer.fill(0);
    for (let i = 0; i < power.length; i += 1) {
      const fin = this.fin(keyName, f0, i);
      const finSamples = InharmonicityModel.sineWave(
        fin,
        sampleRate,
        seconds,
        power[i] / sum
      );
      for (let j = 0; j < finSamples.length; j += 1) {
        buffer[j] += finSamples[j];
      }
    }
    return buffer;
  }
}

export default InharmonicityModel;
