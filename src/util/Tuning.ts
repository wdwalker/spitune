import { KeyName } from './Piano';

/**
 * A tuning for a piano, which is just the fundamental frequency for
 * each key.
 */
type Tuning = Map<KeyName, number>;

export default Tuning;
