const wav = require('node-wav');
import * as fs from 'fs';

import FFT from '../src/util/FFT';
import { KeyName, KEY_NAMES } from '../src/util/Piano';
import Tuning from '../src/util/Tuning';
import EqualTemperament from '../src/util/EqualTemperament';

const DESIRED_PRECISE_HZ_PRECISION = 0.02;
const SAMPLE_RATE = 44100;

let preciseFFTSize = 65536;
while (SAMPLE_RATE / preciseFFTSize > DESIRED_PRECISE_HZ_PRECISION) {
  preciseFFTSize *= 2;
}
const preciseFFT = new FFT(preciseFFTSize, SAMPLE_RATE);
const tuning: Tuning = new Map<KeyName, number>();

const files = fs.readdirSync('../recordings');
files.forEach((file) => {
  const keyName = file.split('.')[0] as KeyName;
  const buffer = fs.readFileSync(`../recordings/${file}`);
  const result = wav.decode(buffer);
  // console.log(new Float32Array(result.channelData[0]));
  preciseFFT.reset();
  preciseFFT.process(result.channelData[0]);

  const minHz = EqualTemperament.hz(keyName, -50);
  const minBin = Math.floor(minHz / preciseFFT.hzPerBin);
  const maxHz = EqualTemperament.hz(keyName, +50);
  const maxBin = Math.floor(maxHz / preciseFFT.hzPerBin);
  let peak = minBin;
  for (let i = minBin; i < maxBin + 1; i += 1) {
    if (preciseFFT.fft.spectrum[i] > preciseFFT.fft.spectrum[peak]) {
      peak = i;
    }
  }
  console.log(file, peak * preciseFFT.hzPerBin);
  tuning[keyName] = peak * preciseFFT.hzPerBin;
});

for (let i = 0; i < KEY_NAMES.length; i += 1) {
  if (KEY_NAMES[i].indexOf('#') > 0) {
    console.log(`  '${KEY_NAMES[i]}': ${tuning[KEY_NAMES[i]]},`);
  } else {
    console.log(`  ${KEY_NAMES[i]}: ${tuning[KEY_NAMES[i]]},`);
  }
}
