const wav = require('node-wav');
import * as fs from 'fs';

import FFT from '../src/util/FFT';
import { KeyName, KEY_NAMES } from '../src/util/Piano';
import InharmonicityModel from '../src/util/InharmonicityModel';

const DESIRED_PRECISE_HZ_PRECISION = 0.02;
const SAMPLE_RATE = 44100;

const model: InharmonicityModel = new InharmonicityModel({});

let preciseFFTSize = 65536;
while (SAMPLE_RATE / preciseFFTSize > DESIRED_PRECISE_HZ_PRECISION) {
  preciseFFTSize *= 2;
}
const preciseFFT = new FFT(preciseFFTSize, SAMPLE_RATE);

const files = fs.readdirSync('../recordings');
files.forEach((file) => {
  const keyName = file.split('.')[0] as KeyName;
  const buffer = fs.readFileSync(`../recordings/${file}`);
  const result = wav.decode(buffer);
  preciseFFT.reset();
  preciseFFT.process(result.channelData[0]);

  model[keyName] = model.generate(keyName, preciseFFT);
  console.log(`  ${keyName}: ${JSON.stringify(model[keyName])},`);
});

console.log();
console.log();
console.log();

console.log('{');
for (let i = 0; i < KEY_NAMES.length; i += 1) {
  if (KEY_NAMES[i].indexOf('#') > 0) {
    console.log(`  '${KEY_NAMES[i]}': ${JSON.stringify(model[KEY_NAMES[i]])},`);
  } else {
    console.log(`  ${KEY_NAMES[i]}: ${JSON.stringify(model[KEY_NAMES[i]])},`);
  }
}
console.log('}');
